<?php 

session_start();
// dirige toi vers la page indiqué dans le paramètre $_GET["page"]

require_once "config/env.php";
require_once "vendor/autoload.php";

// Micro router
// On change les controllers en fonction de la valeur du paramétrage page
// Si le paramètrage page n'est pas renseigné dans l'url, on affiche la page d'accueil du HomeController par défaut.

if(isset($_GET["page"]) && $_GET["page"]){
    $controllerName = ucfirst($_GET["page"]);
    $className = "App\\Controller\\{$controllerName}Controller";

    $page = new $className();

    // Condition ternaire
    $id = (isset($_GET["id"]) && $_GET["id"]) ? $_GET["id"] : null;

    if(isset($_GET["action"]) && $_GET["action"]) {
        $method = $_GET["action"];

        $page->$method($id);
    } else {
        $page->main($id);
    }

    //Si on décompose la condition ternaire en structure conditionnelle classique : 
   // if (isset($_GET["id"]) && $_GET["id"]) {
   //     $id = $_GET["id"];
   // }else{
  //      $id = null;
  //  }

  //var_dump($id);

}else{
    $page = new App\Controller\HomeController();

    $page->main();
}

?>