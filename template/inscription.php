<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inscription
    </title>
    <link rel="stylesheet" href="public/build/app.css">
</head>
<body>


<div class="formulaire_log">
<div class="connect">
<h1>Créer mon compte</h1>
<form action="?page=inscription&action=createPersonne" method="post">
<div class="block">
    <p>Information personnelle</p>
            <div>
                <label for="lname">Nom</label>
                <input type="text" name="lname" id="lname" required>
            </div>
            <div>
                <label for="fname">Prenom</label>
                <input type="text" name="fname" id="fname" required>
            </div>
            <div>
                <!--<label for="age">date de naissance</label>
                <label for="age">Veuillez saisir votre date de naissance :</label>
                <input type="date" id="age" name="age">-->
                <!--<label for="age">Age</label>
                <input type="text" name="age" id="age">
            
            <div>
                <label for="img">Image</label>
                <input type="text" name="img" id="img">
            </div>
            <div>
                <label for="telephone">Telephone:</label>
                    <input type ="tel" name="telephone" maxlength="10"/>
            </div>
            <div>
                <label for="email">Email</label>
                <input type="email" name="email" id="email" size="30" required>
            </div>
            -->
    <p>Information du compte</p>        
            
            <div>
                <label for="identifiant">Identifiant</label>
                <input type="text" name="identifiant" id="identifiant" required>
            </div>
            <div>
                <label for="mdp">Mot de passe</label>
                <input type="password" name="mdp" id="mdp" required>
            </div>
    
            </div>   

        <div>
            <input type="submit" name="btnCreateForm" value="Créer">
        </div>
        <div>
        <a href="./">Retour à la page de connexion</a>
        </div>
</div>
</div>

        
</form>
</body>
</html>