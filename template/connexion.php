<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion site</title>
    <link rel="stylesheet" href="public/build/app.css">
</head>
<body>

<!--
    <?php include "menu.php" ?>

    <?=
        password_hash("bob", PASSWORD_DEFAULT);
    ?>
-->
    <div class="formulaire_log">
        <div class="connect">
        <h1>Bienvenu sur le chat interactif du FIM CCI SAINT-Lô</h1>
        <h2>Connexion</h2>
    
        </div>
        <div class="formulaire">
        <form action="?page=login&action=connexion" method="post">
            <div>
                <label for="identifiant">Identifiant</label>
                <input type="text" name="coIdentifiant" id="identifiant">
            </div>
            <div>
                <label for="mdp">Mot de passe</label>
                <input type="password" name="coMdp" id="mdp">
            </div>
            <div class="button">
                <input type="submit" name="coBtnSubmit" value="Connexion">
            </div>
            <a href="?page=inscription">Pas de compte ? Inscris-toi !</a>
        </form>
        </div>
    </div>
</body>
</html>