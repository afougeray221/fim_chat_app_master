<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion site</title>
    <link rel="stylesheet" href="public/build/app.css">
</head>
<body>

    <?php include "menu.php" ?>
    <h1>Bienvenue 
        <?php 
            if(isset($_SESSION["role"]) && !is_null($_SESSION["role"])) {
                echo $_SESSION["username"];
            } else {
                echo "sur la page d'accueil";
            }
        ?>
    </h1>

    <?php foreach ($groupes as $groupe) { ?>
        <article>
            <h2><?= $groupe->getTitle() ?></h2>
                <div>
                <span><?= $groupe->getAuthor() ?></span><span><?= $groupe->getUpdateDate() ?></span>
                </div>
            <p><?= $groupe->getContent() ?></p>
            <a href="?page=groupe&id=<?= $groupe->getIdGroupe() ?>">Lire plus</a>
        </article>
<?php } ?>
</body>
</html>