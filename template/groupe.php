<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Groupe site</title>
    <link rel="stylesheet" href="public/build/page1.css">
</head>
<body>
<?php include "menu.php" ?>
    <h1><?= $groupe->getTitle()?></h1>
    <div>
        <span>Par<?= $groupe->getAuthor()?> -</span>
        <span>Créé le<?= $groupe->getCreateDate()?> -</span>
        <span>Dernière modification le <?= $groupe->getUpdateDate()?> -</span>
    </div>
    <p><?= $groupe->getContent()?></p>
    <section>
    <h2>message</h2>
    <?php foreach ($messages as $message) { ?>
        <div style="margin-top: 24px">
        <strong><?= $message->getAuthor()?></strong> - <span><?= $message->getDate() ?></span>
        <p><?= $message->getContent() ?></p>
        </div>
    <?php } ?>
    </section>
</body>
</html>
