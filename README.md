# FIM BLOG

FIM Blog est un projet de micro CMS pour réaliser un blog en PHP. 

## Pattern du projet 
Pour ce projet, on utilisera le pattern *MVC* (Model, View, Controller).
Le *Model* est la partie qui permet la gestion des données, dans ce projet ce sera dans src/Repository.
La *View* est la partie qui permet la gestion des vues, donc de l'affichage des données pour l'utilisateur, dans ce projet ce sera dans *templates*. 
Le *Controller* est la partie qui permet la gestion de la logique, dans ce projet ce sera dans src/Controller.

## Comment démarrer le projet 
Après avoir cloner le projet, il faut installer les dépendances et l'autoloading avec composer.

```bash
composer install
yarn install
```

## Utiliser Webpack Encore
Lorsque l'on fait du style ou du JS sur ce projet, il faut le compiler. 
Pour les compiler, on utilisera les commandes suivantes : 

```bash
# Compiler un one shot en mode developpeur (il va tous conserver, les commentaires, les sautes de lignes, ...)
yarn encore dev
```

```bash
# Compiler un one shot en mode production (il va minifier le code de manière en optimiser les chargement de page)
yarn encore poduction
```

```bash
# Compiler en mode watch (c'est à dire en continue, tout va s'ajouter automatiquement)
yarn encore dev --watch

# Le mode watch permet de repiler le code automatiquement dès qu'il détecte une modification dans les fichiers du dossier asset
```