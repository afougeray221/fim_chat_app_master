<?php 

namespace App\Entity;

class Personne
{
    public $idPersonne;
    public $email;
    public $identifiant;
    public $fname;
    public $lname;
    public $mdp;
    public $img;
    public $telephone;
    public $age;
    public $role;

    


    /**
     * Get the value of idPersonne
     */ 
    public function getIdPersonne()
    {
        return $this->id_user;
    }

    /**
     * Set the value of idPersonne
     *
     * @return  self
     */ 
    public function setIdPersonne($id_user)
    {
        $this->id_user = $id_user;

        return $this;
    }
    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }
    /**
     * Get the value of identifiant
     */ 
    public function getIdentifiant()
    {
        return $this->identifiant;
    }

    /**
     * Set the value of identifiant
     *
     * @return  self
     */ 
    public function setIdentifiant($identifiant)
    {
        $this->identifiant = $identifiant;

        return $this;
    }

    /**
     * Get the value of fname
     */ 
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set the value of fname
     *
     * @return  self
     */ 
    public function setFname($fname)
    {
        $this->fname = $fname;

        return $this;
    }

    /**
     * Get the value of lname
     */ 
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set the value of lname
     *
     * @return  self
     */ 
    public function setLname($lname)
    {
        $this->lname = $lname;

        return $this;
    }

    /**
     * Get the value of mdp
     */ 
    public function getMdp()
    {
        return $this->mdp;
    }

    /**
     * Set the value of mdp
     *
     * @return  self
     */ 
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;

        return $this;
    }

    /**
     * Get the value of img
     */ 
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Set the value of img
     *
     * @return  self
     */ 
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Get the value of telephone
     */ 
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set the value of telephone
     *
     * @return  self
     */ 
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get the value of age
     */ 
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set the value of age
     *
     * @return  self
     */ 
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

        /**
     * Get the value of role
     */ 
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set the value of role
     *
     * @return  self
     */ 
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    

    public function __construct(Array $data = [])
    {
        if(!empty($data)){

            $this->setIdPersonne($data["id_personne"]);
            $this->setIdentifiant($data["identifiant"]);
            $this->setEmail($data["email"]);
            $this->setFname($data["fname"]);
            $this->setLname($data["lname"]);
            $this->setMdp($data["mdp"]);
            $this->setImg($data["img"]);
            $this->setTelephone($data["telephone"]);
            $this->setAge($data["age"]);
            $this->setRole($data["role"]);
            
        }
    }




}