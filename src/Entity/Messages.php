<?php

namespace App\Entity;

class Messages
{
    public $idMessages;
    public $author;
    public $date;
    public $content;
    public $visibility;
    public $id_groupe;

    /**
     * Get the value of idMessages
     */ 
    public function getIdMessages()
    {
        return $this->idMessages;
    }

    /**
     * Set the value of idMessages
     *
     * @return  self
     */ 
    public function setIdMessages($idMessages)
    {
        $this->idMessages = $idMessages;

        return $this;
    }

    /**
     * Get the value of author
     */ 
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set the value of author
     *
     * @return  self
     */ 
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get the value of date
     */ 
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */ 
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the value of content
     */ 
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set the value of content
     *
     * @return  self
     */ 
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get the value of visibilite
     */ 
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * Set the value of visibilite
     *
     * @return  self
     */ 
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * Get the value of id_groupe
     */ 
    public function getIdGroupe()
    {
        return $this->id_groupe;
    }

    /**
     * Set the value of id_groupe
     *
     * @return  self
     */ 
    public function setIdGroupe($id_groupe)
    {
        $this->id_groupe = $id_groupe;

        return $this;
    }

    // Constructeur
    public function __construct(Array $data = [])
    {
        if(!empty($data)){
            $this->setIdMessages($data["id_message"]);
            $this->setAuthor($data["auteur"]);
            $this->setDate($data["date"]);
            $this->setContent($data["contenu"]);
            $this->setVisibility($data["visibilite"]);
            $this->setIdGroupe($data["id_Groupe"]);
        }
    }
}
