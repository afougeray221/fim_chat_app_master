<?php

namespace App\Entity;

class UserGrp
{
    public $id_usergrp;
    public $id_user;
    public $id_groupe;

    /**
     * Get the value of idMessages
     */ 
    public function getIdUserGrp()
    {
        return $this->id_usergrp;
    }

    /**
     * Set the value of idMessages
     *
     * @return  self
     */ 
    public function setIdUserGrp($id_usergrp)
    {
        $this->id_usergrp = $id_usergrp;

        return $this;
    }

    /**
     * Get the value of id_groupe
     */ 
    public function getIdGroupe()
    {
        return $this->id_groupe;
    }

    /**
     * Set the value of id_groupe
     *
     * @return  self
     */ 
    public function setIdGroupe($id_groupe)
    {
        $this->id_groupe = $id_groupe;

        return $this;
    }

    /**
     * Get the value of id_user
     */ 
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * Set the value of id_user
     *
     * @return  self
     */ 
    public function setIdUser($id_user)
    {
        $this->id_user = $id_user;

        return $this;
    }
    // Constructeur
    public function __construct(Array $data = [])
    {
        if(!empty($data)){
            $this->setIdUserGrp($data["id_message"]);
            $this->setIdGroupe($data["id_Groupe"]);
            $this->setIdUser($data["id_User"]);
        }
    }
}
