<?php

namespace App\Repository;

use App\Entity\Personne;
use PDO;
use PDOException;

class PersonneRepository extends ManagerRepository
{
    public function pLogin(Personne $p)
    {
        $sql = "SELECT * FROM personne WHERE identifiant=:identifiant LIMIT 1";

        try {
            $co = $this->dbConnexion();
            $identifiant = $p->getIdentifiant();
            $mdp = $p->getMdp();

            $stmt = $co->prepare($sql);
            $stmt->bindParam(":identifiant", $identifiant);
            $stmt->execute();

            $data = $stmt->fetch(PDO::FETCH_ASSOC);

            var_dump($data);
            if($stmt->rowCount() > 0) {

                if(password_verify($p->getMdp(), $data["mdp"])){
                    // connecté l'utilisateur 
                    $_SESSION["role"] = intval($data["role"]);
                    $_SESSION["identifiant"] = $data["identifiant"];

                    header('Location: ?page=conversation');
                } else {
                    echo "mdp incorrect";
                }
            } else {
                echo "identifiant incorrect";
            }

        } catch (PDOException $e) {
            (new \Master\Logger\Logger())->logError(DATE_RFC2822, $e, 3, ERROR_LOG_FILE);
        }
    }

    public function pLogout()
    {
        session_destroy();
        unset($_SESSION["role"]);
        unset($_SESSION["identifiant"]);
    }

    public function addPersonne(Personne $p) {
        $sql = "INSERT INTO personne (identifiant, lname, fname, mdp, role) 
                VALUES (:identifiant, :lname, :fname, :mdp, :role)";

        $identifiant = $p->getIdentifiant();
        $lname = $p->getLname();
        $fname = $p->getFname();
        $mdp = password_hash($p->getMdp(), PASSWORD_DEFAULT);
        $role = $p->getRole();
    

        try {
            $co = $this->dbConnexion();

            $stmt = $co->prepare($sql);

            $stmt->bindParam(":identifiant", $identifiant);
            $stmt->bindParam(":lname", $lname);
            $stmt->bindParam(":fname", $fname);
            $stmt->bindParam(":mdp", $mdp);
            $stmt->bindParam(":role", $role);

            $stmt->execute();

            $this->dbDeconnexion($co);
        } catch(PDOException $e) {
            (new \Master\Logger\Logger())->logError(DATE_RFC2822, $e, 3, ERROR_LOG_FILE);
        }
    }
}