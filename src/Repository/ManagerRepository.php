<?php

namespace App\Repository;

use PDO;

/**
 * Manager Repository
 * 
 * Le manager repository nous permettra de gérer la connection et la deconnexion à nos bases de données
 */

class ManagerRepository 
{

    private $servername = "localhost";
    private $dbName = "chatapp";
    private $port = "8889";
    private $username = "root";
    private $password = "root";

    /**
     * Database connection
     * 
     * Permet la conection à une base de données.
     */

    public function dbConnexion()
    {
        $co = new PDO(
            "mysql:host={$this->servername};dbname={$this->dbName};port={$this->port};charset=utf8",
            $this->username,
            $this->password
        );

        $co->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $co;
    }

    /**
     * Database Déconnexion
     * 
     * Permet de se déconnecter d'une base de données.
     */

    public function dbDeconnexion($co)
    {
        $co = null;
    }
}