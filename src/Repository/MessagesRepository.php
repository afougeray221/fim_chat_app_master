<?php 

namespace App\Repository;

use App\Entity\Messages;
use PDO;
use PDOException;

class MessagesRepository extends ManagerRepository
{
    public function getAllMessagesByGroupe($id_groupe)
    {
        $objects = [];

        try {
            $co = $this->dbConnexion();

            $sql = "SELECT * FROM message WHERE id_groupe=:id";

            $stmt = $co->prepare($sql);
            $stmt->bindParam(":id", $id_groupe);
            $stmt->execute();

            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $objects[] = new Messages($row);
            }

            $this->dbDeconnexion($co);

        } catch (PDOException $e) {
            echo $e;
        }
        return $objects;
    }
}
