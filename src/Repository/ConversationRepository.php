<?php

namespace App\Repository;

use App\Entity\UserGrp;
use PDO;
use PDOException;

class ConversationRepository extends ManagerRepository
{
    public function getAllUserGroupe()
    {
        $objects = [];

        try{

            // Connexion à la bdd
            $co = $this->dbConnexion();

            // Requête SQL que l'on veut réaliser
            $sql = "SELECT * FROM user_grp ORDER BY id_usergrp DESC";
            
            // Préparation et exécution de la requêt SQL
            $stmt = $co->prepare($sql);
            $stmt->execute();
            
            // Récupération des données
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                $objects[] = new UserGrp($row);

            }

            // Fermeture de la connexion à la bdd
            $this->dbDeconnexion($co);
        } catch (PDOException $e) {
            echo $e;
        }

        return $objects;
    }
    public function getOneUserGroupe($id_usergrp)
    {
        try {
            $co = $this->dbConnexion();
            $sql = "SELECT * FROM user_grp WHERE id_usergrp=:id";

            $stmt = $co->prepare($sql);
            $stmt->bindParam(":id", $id_usergrp);
            $stmt->execute();

            $data = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->dbDeconnexion($co);
        } catch (PDOException $e) {
            echo $e;
        }
        return new UserGrp($data);
    }
}

