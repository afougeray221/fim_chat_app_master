<?php

namespace App\Repository;

use App\Entity\Groupe;
use PDO;
use PDOException;

class GroupeRepository extends ManagerRepository
{
    public function getAllGroupe()
    {
        $objects = [];

        try{

            // Connexion à la bdd
            $co = $this->dbConnexion();

            // Requête SQL que l'on veut réaliser
            $sql = "SELECT * FROM groupe ORDER BY id_groupe DESC";
            
            // Préparation et exécution de la requêt SQL
            $stmt = $co->prepare($sql);
            $stmt->execute();
            
            // Récupération des données
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                $objects[] = new Groupe($row);

            }

            // Fermeture de la connexion à la bdd
            $this->dbDeconnexion($co);
        } catch (PDOException $e) {
            echo $e;
        }

        return $objects;
    }
    public function getOneGroupe($idGroupe)
    {
        try {
            $co = $this->dbConnexion();
            $sql = "SELECT * FROM groupe WHERE id_groupe=:id";

            $stmt = $co->prepare($sql);
            $stmt->bindParam(":id", $idGroupe);
            $stmt->execute();

            $data = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->dbDeconnexion($co);
        } catch (PDOException $e) {
            echo $e;
        }
        return new Groupe($data);
    }
}

