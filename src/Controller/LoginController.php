<?php

namespace App\Controller;

use App\Entity\Personne;
use App\Repository\PersonneRepository;

class LoginController
{
    public function main()
    {
        include "template/connexion.php";
    }

    public function connexion()
    {
        if(isset($_POST["coBtnSubmit"]) && !is_null($_POST["coBtnSubmit"])) {
            $p = new Personne();

            $p ->setIdentifiant($_POST["coIdentifiant"]);
            $p ->setMdp($_POST["coMdp"]);


            $pRepo = new PersonneRepository();
            $pRepo->pLogin($p);

           if(isset($_SESSION["role"]) && !is_null($_SESSION["role"])) {
                if($_SESSION["role"] === 1 || $_SESSION["role"] === 3 ) {
                    // TODO : rediriger vers l'admin
                    header('Location: ?page=conversation');
                } elseif ($_SESSION["role"] === 3) {
                    header('Location: ?page=conversation');
                }
                include "template/conversation.php";
            }
        }
    }

    public function deconnexion ()
    {
        $pRepo = new PersonneRepository();
        $pRepo->pLogout();

        header('Location: ?page=home');

        
    }
}