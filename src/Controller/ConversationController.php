<?php

namespace App\Controller;

use App\Repository\GroupeRepository;
use App\Repository\MessagesRepository;
use App\Repository\PersonneRepository;
use App\Repository\ConversationRepository;

use App\Entity\Personne;

class ConversationController
{
    // Méthode pour afficher un article à l'utilisateur
    public function main($id_usergrp)
    {

        include "template/conversation.php";
    }
}
