<?php

namespace App\Controller;

use App\Repository\GroupeRepository;
use App\Repository\MessagesRepository;

class GroupeController
{
    // Méthode pour afficher un article à l'utilisateur
    public function main($idGroupe)
    {

        $aRepo = new GroupeRepository();
        $groupe = $aRepo->getOneGroupe($idGroupe);

        $cRepo = new MessagesRepository();
        $messages = $cRepo->getAllMessagesByGroupe($groupe->getIdGroupe());

        include "template/groupe.php";
    }
}
