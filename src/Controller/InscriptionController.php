<?php 

namespace App\Controller;

use App\Entity\Personne;
use App\Repository\PersonneRepository;

class InscriptionController
{
    public function main()
    {


        include "template/inscription.php";
    }

    public function createForm()
    {
        include "template/inscription.php";

        
    }

    public function createPersonne()
    {

        if (isset($_POST["btnCreateForm"]) && !is_null($_POST["btnCreateForm"])) {
            $p = new Personne();
            $p->setIdentifiant($_POST["identifiant"]);
            $p->setLname($_POST["lname"]);
            $p->setFname($_POST["fname"]);
            $p->setMdp($_POST["mdp"]);
            $p->setRole(3);
            
            $pRepo = new PersonneRepository();
            $pRepo->addPersonne($p);

            header("Location: ?page=home");
        }
        
    }
}
