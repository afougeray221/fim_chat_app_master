<?php
// on met Logger avec une majuscule car c'est une classe

namespace Master\Logger;

class Logger
{
    public function logError($dateFormat, $e, $messageType = 0, $destination)
    {
        $now = date($dateFormat);
        error_log(
            "{$now} : {$e->getFile()} - {$e->getLine()}\n Message : {$e->getMessage()}\n",
            $messageType,
            $destination
        );
    }
}