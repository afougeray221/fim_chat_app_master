-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : mer. 09 juin 2021 à 15:13
-- Version du serveur :  5.7.24
-- Version de PHP : 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `chatapp`
--

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

CREATE TABLE `groupe` (
  `id_groupe` int(11) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `date_creation` datetime NOT NULL,
  `image` varchar(255) NOT NULL,
  `contenu` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `msg_id` int(11) NOT NULL,
  `heure` time NOT NULL,
  `statut_lecture` int(11) NOT NULL,
  `contenu` text CHARACTER SET utf8 NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `message`
--

INSERT INTO `message` (`msg_id`, `heure`, `statut_lecture`, `contenu`, `id_user`) VALUES
(3, '00:00:00', 1, 'coucou les gens', 1),
(4, '00:00:00', 1, 'un test simple', 2);

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

CREATE TABLE `personne` (
  `user_id` int(11) NOT NULL,
  `identifiant` varchar(50) NOT NULL,
  `role` tinyint(4) NOT NULL,
  `fname` varchar(255) CHARACTER SET utf8 NOT NULL,
  `lname` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `mdp` varchar(255) CHARACTER SET utf8 NOT NULL,
  `img` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `telephone` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `age` varchar(3) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `personne`
--

INSERT INTO `personne` (`user_id`, `identifiant`, `role`, `fname`, `lname`, `email`, `mdp`, `img`, `telephone`, `age`) VALUES
(1, 'alice', 0, 'Alice', 'Fougeray', 'afougeray221@gmail.com', 'f56ddcc750aca00f37a9c64094af0d9d', '16179748201575301902904.jpeg', '', ''),
(2, 'bob', 0, 'bob', 'bob', 'bob@bob.fr', '$2y$10$GLB6bNzg4/HvHkLrWesvQ.6QDIlyWM6Elb7SXEFine5gxFUQX1eea', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user_grp`
--

CREATE TABLE `user_grp` (
  `id_usergrp` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `message` int(11) NOT NULL,
  `groupe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `groupe`
--
ALTER TABLE `groupe`
  ADD PRIMARY KEY (`id_groupe`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`msg_id`),
  ADD KEY `FK_USER_GRP_USER` (`id_user`);

--
-- Index pour la table `personne`
--
ALTER TABLE `personne`
  ADD PRIMARY KEY (`user_id`);

--
-- Index pour la table `user_grp`
--
ALTER TABLE `user_grp`
  ADD PRIMARY KEY (`id_usergrp`),
  ADD KEY `FK_USER_GRP_USERS` (`user`),
  ADD KEY `FK_USER_GRP_MESSAGES` (`message`),
  ADD KEY `FK_USER_GRP_GROUPS` (`groupe`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `groupe`
--
ALTER TABLE `groupe`
  MODIFY `id_groupe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `msg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `personne`
--
ALTER TABLE `personne`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `user_grp`
--
ALTER TABLE `user_grp`
  MODIFY `id_usergrp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_USER_GRP_USER` FOREIGN KEY (`id_user`) REFERENCES `personne` (`user_id`);

--
-- Contraintes pour la table `user_grp`
--
ALTER TABLE `user_grp`
  ADD CONSTRAINT `FK_USER_GRP_GROUPS` FOREIGN KEY (`groupe`) REFERENCES `groupe` (`id_groupe`),
  ADD CONSTRAINT `FK_USER_GRP_MESSAGES` FOREIGN KEY (`message`) REFERENCES `message` (`msg_id`),
  ADD CONSTRAINT `FK_USER_GRP_USERS` FOREIGN KEY (`user`) REFERENCES `personne` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
